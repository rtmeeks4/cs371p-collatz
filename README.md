# CS371p: Object-Oriented Programming Collatz Repo

* Name: Rob Meeks

* EID: rtm893

* GitLab ID: rtmeeks4

* HackerRank ID: rtmeeks4

* Git SHA: aca5ab1a32dadd577801dfe50d07ae609a048e14

* GitLab Pipelines: https://gitlab.com/rtmeeks4/cs371p-collatz/pipelines

* Estimated completion time: 10

* Actual completion time: 12

* Comments: Most of the extra time was from technical issues instead of actual coding, should hopefully trim down work time next time
