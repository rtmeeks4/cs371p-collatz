
#include <cstdlib>
#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

int main () {
    for(int i=0; i<200; ++i)
    	printf("%d %d\n", rand()%1000000, rand()%1000000);
    return 0;
}