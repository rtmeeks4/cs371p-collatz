#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

using namespace std;

int collatz_eval (int i, int j) { //same as the eval I used at the time of making this code
	int m;
	long temp;
	if(i>j){
		m=i/2+1;
		temp = i;
		i=m>j?m:j;
		j=temp;
	}
	else{
		m=j/2+1;
		i=m>i?m:i;
	}
    int max=0;
    int count=0;
    for(int k=i; k<=j; ++k){
    	count=1;
    	temp=k;
    	while(temp!=1){
    		if(temp%2==0){
    			temp/=2;
    			count++;
    		}
    		else{
    			temp+=((temp>>1)+1);
    			count+=2;
    		}
    	}
    	if(count>max){
    		max=count;
    	}
    }
    assert(max>=1);
    return max;
    //return i + j;
}

int main(int argc, char const *argv[])
{

	for(int i=0; i<1000; ++i){
		printf("%d, ",collatz_eval((i*1000+1),((i+1)*1000))); //each loop finds the best of every 1000 values and prints it for copying to metaarray in Collatz
	}
		printf("\n");
	return 0;
}